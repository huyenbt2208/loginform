
import 'antd/dist/antd.css';
import './Login.css';
import React from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
//import { UserOutlined, LockOutlined } from '@ant-design/icons';



//const { Form, Input, Button, Checkbox } = antd;

const Login = () => {
    const onFinish = (values: any) => {
        console.log('Received values of form: ', values);
    };

    return (
        <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            onFinish={onFinish}
        >
            <h3 className="lgTitle">Login Form</h3>
            <Form.Item
                name="username"
                rules={[{ required: true, message: 'Please input your Username!' }]}
            >
                <Input className="site-form-item-icon" placeholder="Username" />
            </Form.Item>
            <Form.Item
                name="password"
                rules={[{ required: true, message: 'Please input your Password!' }]}
            >
                <Input
                    className="site-form-item-icon"
                    type="password"
                    placeholder="Password"
                />
            </Form.Item>
            <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <a className="login-form-forgot" href="">
                    Forgot password
        </a>
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit" className="login-form-button">
                    Log in
        </Button>
        Or <a href="">register now!</a>
            </Form.Item>
        </Form>
    );
};
export default Login;
