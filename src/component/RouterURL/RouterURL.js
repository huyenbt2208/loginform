import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    NavLink,
    Routes
} from "react-router-dom";
import RegistrationForm from '../RegistrationForm';
import Login from '../Login';


const RouterURL = () => {
    return (
        <div>
            <div>
                <Routes>
                    <Route path="/login" Component={Login} />
                    <Route path="/registerform" Component={RegistrationForm} />
                </Routes>
            </div>
        </div>
    );
}

export default RouterURL;