import React from "react";
import { Menu, Layout } from "antd";

const { SubMenu } = Menu;

export default function Sider(props) {
    const { handleClick } = props;
    return (
        <Layout.Sider>
            <Menu theme="dark" mode="inline" openKeys={"sub1"}>
                <SubMenu
                    key="sub1"
                    title={
                        <span>
                            <span>Navigation One</span>
                        </span>
                    }
                >
                    <Menu.Item key="1" onClick={handleClick}>
                        Login
                    </Menu.Item>
                    <Menu.Item key="2" onClick={handleClick}>
                        Register
                    </Menu.Item>
                    
                </SubMenu>
            </Menu>
        </Layout.Sider>
    );
}
