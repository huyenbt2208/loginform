﻿import React, { useState } from "react";
import './App.css';
import Sider from "./Sider";
import { Layout, Content } from "antd";
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";
import Login from "./component/Login";
import Register from "./component/RegistrationForm";

function App() {
    const style = {
        fontSize: "30px",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    };

    const components = {
        1: <Login />,
        2: <Register />
    };

    const [render, updateRender] = useState(1);

    const handleMenuClick = menu => {
        updateRender(menu.key);
    };
    
    return (
        <Router>
            <div className="App">
                <Layout style={{ minHeight: "100vh" }}>
                    <Sider handleClick={handleMenuClick} />
                    <Layout>
                        <div className="lgForm">{components[render]}</div>
                    </Layout>
                </Layout>
          
            </div>
        </Router>
  );
};

export default App;
