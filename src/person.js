import './App.css';

const Person = (props) => {
    return (
        <div className="Person">
            <p>{props.name}</p><input type="text" onChange={props.changed} value={props.name}></input>
        </div>
    );
};

export default Person;
